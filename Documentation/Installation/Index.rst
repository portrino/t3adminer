.. include:: /Includes.rst.txt

.. _installation:

Installation
============

The extension can be installed by the usual methods:

#. **Use composer**: Run

   .. code-block:: bash

      composer require jigal/t3adminer

   In your TYPO3 installation.

#. **Get it from the Extension Manager:** Switch to the module :guilabel:`Admin Tools > Extensions`.
   Switch to :guilabel:`Get Extensions` and search for the extension key
   *t3adminer* and import the extension from the repository.

#. **Get it from typo3.org:** You can always get current version from `TER`_
   by downloading the zip version. Upload the file afterwards in the Extension
   Manager.

More detailed information on how to install extensions can be found in the `Getting Started`_ tutorial.

.. _TER: https://extensions.typo3.org/extension/t3adminer
.. _Getting Started: https://docs.typo3.org/m/typo3/tutorial-getting-started/main/en-us/Extensions/Index.html